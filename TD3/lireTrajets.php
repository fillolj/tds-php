<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Test trajet</title>
		<meta charset="UTF-8">
	</head>
	<body>
        <ul>
            <?php
            require_once 'Trajet.php';
            foreach(Trajet::getTrajets() as $trajet) {
                $trajetId = $trajet->getId();

                echo "<li>$trajet";
                $passagers = $trajet->getPassagers();
                if($passagers != null) {
                    echo '<ul>';
                    foreach($trajet->getPassagers() as $passager) {
                        $login = $passager->getLogin();
                        echo "<li>$passager (<a href=\"supprimerPassager.php?trajet_id=$trajetId&login=$login\">supprimer</a>)</li>";
                    }
                    echo '</ul>';
                }
                echo '</li>';
            }
            ?>
        </ul>
	</body>
</html>
