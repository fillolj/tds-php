<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Test utilisateur</title>
		<meta charset="UTF-8">
	</head>
	<body>
        <ul>
            <?php
            require_once 'Trajet.php';

            $login = $_GET['login'];
            $trajetId = $_GET['trajet_id'];

            $trajet = Trajet::recupererTrajetParId($trajetId);
            if($trajet == null) {
                echo "<p>Le trajet $trajetId n'existe pas.</p>";
            } elseif($trajet->supprimerPassager($login)) {
                echo "<p>Utilisateur $login supprimé du trajet $trajetId.</p>";
            } else {
                echo "<p>Impossible de supprimer l'utilisateur $login du trajet $trajetId.</p>";
            }
            ?>
        </ul>
	</body>
</html>
