<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';
class Utilisateur {
	private string $login;
	private string $nom;
	private string $prenom;
	private ?array $trajetsCommePassager;

	public function getLogin() : string {
		return $this->login;
	}

	public function setLogin(string $login) {
		$this->login = substr($login, 0, 64);
	}

	public function getNom() : string {
		return $this->nom;
	}

	public function setNom(string $nom) {
		$this->nom = $nom;
	}

	public function getPrenom() : string {
		return $this->prenom;
	}

	public function setPrenom(string $prenom) {
		$this->prenom = $prenom;
	}

	public function getTrajetsCommePassager() : array {
		if($this->trajetsCommePassager == null) {
			$this->setTrajetsCommePassager($this->recupererTrajetsCommePassager());
		}
		return $this->trajetsCommePassager;
	}

	public function setTrajetsCommePassager(?array $trajetsCommePassager) {
		$this->trajetsCommePassager = $trajetsCommePassager;
	}

	public function __construct(string $login, string $nom, string $prenom) {
		$this->setLogin($login);
		$this->setNom($nom);
		$this->setPrenom($prenom);
		$this->setTrajetsCommePassager(null);
	}

	public function __toString() : string {
		return "$this->prenom $this->nom";
	}
	
	public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
		return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
	}

	public static function getUtilisateurs() : array {
		$utilisateurs = [];

		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('select login, nom, prenom from utilisateur');
		foreach($pdoStatement as $utilisateurFormatTableau) {
			$utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
		}

		return $utilisateurs;
	}

	public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
		$sql = "SELECT * from utilisateur WHERE login = :loginTag";
		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
		$values = [ "loginTag" => $login ];
		$pdoStatement->execute($values);
		$utilisateurFormatTableau = $pdoStatement->fetch();
		if($utilisateurFormatTableau == false) {
			return null;
		}
		return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
	}

	public function ajouter() : bool {
		try {
			$sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
			$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
			$values = [
				"login" => $this->login,
				"nom" => $this->nom,
				"prenom" => $this->prenom,
			];
			$pdoStatement->execute($values);
			return true;
		} catch (PDOException $e) {
			return false;
		}
	}

	private function recupererTrajetsCommePassager() : array {
		$sql = "SELECT * from trajet JOIN passager ON passager.trajetId = trajet.id WHERE passagerLogin = :login";
		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
		$values = [ "login" => $this->login ];
		$pdoStatement->execute($values);

		$trajets = [];

		foreach($pdoStatement as $trajetFormatTableau) {
			$trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
		}
		return $trajets;
	}
}
