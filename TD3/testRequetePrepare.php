<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Test utilisateur</title>
		<meta charset="UTF-8">
	</head>
	<body>
        <?php
		require_once 'Utilisateur.php';
		$login = "freemang";
		$nom = "Freeman";
		$prenom = "Gordon";
		$utilisateur = Utilisateur::recupererUtilisateurParLogin($login);
		if($utilisateur == null) {
			echo "<p>L'utilisateur n'existe pas. Création de l'utilisateur...</p>";
			$utilisateur = new Utilisateur($login, $nom, $prenom);
			$utilisateur->ajouter();
		}
		echo "<p>$utilisateur</p>";
		?>
	</body>
</html>
