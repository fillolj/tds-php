<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Test trajet</title>
		<meta charset="UTF-8">
	</head>
	<body>
		<?php
		require_once 'Trajet.php';
		$trajet = new Trajet(
			null,
			$_POST['depart'],
			$_POST['arrivee'],
			new DateTime($_POST["date"]),
			$_POST['nbPlaces'],
			$_POST['prix'],
			Utilisateur::recupererUtilisateurParLogin($_POST["conducteurLogin"]),
			isset($_POST['nonFumeur'])
		);
		$trajet->ajouter();
		echo $trajet;
		?>
	</body>
</html>
