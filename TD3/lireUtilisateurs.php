<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Test utilisateur</title>
		<meta charset="UTF-8">
	</head>
	<body>
        <ul>
            <?php
            require_once 'Utilisateur.php';
            foreach(Utilisateur::getUtilisateurs() as $utilisateur) {
                echo "<li>$utilisateur";

                $trajets = $utilisateur->getTrajetsCommePassager();
                if(!empty($trajets)) {
                    echo '<ul>';
                    foreach($utilisateur->getTrajetsCommePassager() as $trajet) {
                        echo "<li>$trajet</li>";
                    }
                    echo '</ul>';
                }

                echo '</li>';
            }
            ?>
        </ul>
	</body>
</html>
