<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';
class Trajet {
	private ?int $id;
	private string $depart;
	private string $arrivee;
	private DateTime $date;
	private int $nbPlaces;
	private int $prix;
	private Utilisateur $conducteur;
	private bool $nonFumeur;

	private array $passagers;

	public function __construct(?int $id, string $depart, string $arrivee, DateTime $date, int $nbPlaces, int $prix, Utilisateur $conducteur, bool $nonFumeur) {
		$this->id = $id;
		$this->depart = $depart;
		$this->arrivee = $arrivee;
		$this->date = $date;
		$this->nbPlaces = $nbPlaces;
		$this->prix = $prix;
		$this->conducteur = $conducteur;
		$this->nonFumeur = $nonFumeur;
		$this->passagers = [];
	}

	public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
		$trajet = new Trajet(
			$trajetTableau["id"],
			$trajetTableau["depart"],
			$trajetTableau["arrivee"],
			new DateTime($trajetTableau["date"]),
			$trajetTableau["nbPlaces"],
			$trajetTableau["prix"],
			Utilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]),
			$trajetTableau["nonFumeur"]
		);
		$trajet->setPassagers($trajet->recupererPassagers());
		return $trajet;
	}

	public function getId() : ?int {
		return $this->id;
	}
	public function setId(?int $id) : void {
		$this->id = $id;
	}

	public function getDepart() : string {
		return $this->depart;
	}
	public function setDepart(string $depart) : void {
		$this->depart = $depart;
	}

	public function getArrivee() : string {
		return $this->arrivee;
	}
	public function setArrivee(string $arrivee) : void {
		$this->arrivee = $arrivee;
	}

	public function getDate() : string {
		return $this->date;
	}
	public function setDate(string $date) : void {
		$this->date = $date;
	}

	public function getNbPlaces() : int {
		return $this->nbPlaces;
	}
	public function setNbPlaces(int $nbPlaces) : void {
		$this->nbPlaces = $nbPlaces;
	}

	public function getPrix() : int {
		return $this->prix;
	}
	public function setPrix(int $prix) : void {
		$this->prix = $prix;
	}

	public function getConducteur() : Utilisateur {
		return $this->conducteur;
	}
	public function setConducteur(Utilisateur $conducteur) : void {
		$this->conducteur = $conducteur;
	}

	public function isNonFumeur() : bool {
		return $this->nonFumeur;
	}
	public function setNonFumeur(bool $nonFumeur) : void {
		$this->nonFumeur = $nonFumeur;
	}

	public function getPassagers() : array {
		return $this->passagers;
	}
	public function setPassagers(array $passagers) : void {
		$this->passagers = $passagers;
	}

	public function __toString() {
		$nonFumeur = $this->nonFumeur ? " non fumeur" : "";
		return "Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).";
	}

	/**
	 * @return Trajet[]
	 */
	public static function getTrajets() : array {
		$pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

		$trajets = [];
		foreach($pdoStatement as $trajetFormatTableau) {
			$trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
		}

		return $trajets;
	}

	public function ajouter() : void {
		$sql = "INSERT INTO trajet (depart, arrivee, date, nbPlaces, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :nbPlaces, :prix, :conducteurLogin, :nonFumeur)";
		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
		$values = [
			"depart" => $this->depart,
			"arrivee" => $this->arrivee,
			"date" => $this->date->format('Y-m-d'),
			"nbPlaces" => $this->nbPlaces,
			"prix" => $this->prix,
			"conducteurLogin" => $this->conducteur->getLogin(),
			"nonFumeur" => $this->nonFumeur ? 1 : 0,
		];
		$pdoStatement->execute($values);
		$this->setId($pdoStatement->lastInsertId());
	}

	private function recupererPassagers() : array {
		$pdoStatement = ConnexionBaseDeDonnees::getPDO()->prepare("SELECT login, prenom, nom FROM passager JOIN utilisateur ON passagerLogin = login WHERE trajetId = :id");
		$pdoStatement->execute(['id' => $this->id]);

		$utilisateurs = [];
		foreach($pdoStatement as $utilisateurFormatTableau) {
			$utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
		}

		return $utilisateurs;
	}

	public static function recupererTrajetParId(int $id) : ?Trajet {
		$sql = "SELECT * from trajet WHERE id = :id";
		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
		$values = [ "id" => $id ];
		$pdoStatement->execute($values);
		$trajetFormatTableau = $pdoStatement->fetch();
		if($trajetFormatTableau == false) {
			return null;
		}
		return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
	}

	public function supprimerPassager(string $passagerLogin): bool {
		$sql = "DELETE FROM passager WHERE trajetId = :id AND passagerLogin = :login";
		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
		$values = [ "id" => $this->getId(), "login" => $passagerLogin ];
		$pdoStatement->execute($values);
		return $pdoStatement->rowCount() > 0;
	}
}
