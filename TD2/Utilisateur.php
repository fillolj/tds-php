<?php
require_once 'ConnexionBaseDeDonnees.php';
class Utilisateur {
	private string $login;
	private string $nom;
	private string $prenom;

	public function getLogin() : string {
		return $this->login;
	}

	public function setLogin(string $login) {
		$this->login = substr($login, 0, 64);
	}

	public function getNom() : string {
		return $this->nom;
	}

	public function setNom(string $nom) {
		$this->nom = $nom;
	}

	public function getPrenom() : string {
		return $this->prenom;
	}

	public function setPrenom(string $prenom) {
		$this->prenom = $prenom;
	}

	public function __construct(string $login, string $nom, string $prenom) {
		$this->setLogin($login);
		$this->setNom($nom);
		$this->setPrenom($prenom);
	}

	public function __toString() : string {
		return "$this->prenom $this->nom";
	}
	
	public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
		return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
	}

	public static function getUtilisateurs() : array {
		$utilisateurs = [];

		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('select login, nom, prenom from utilisateur');
		foreach($pdoStatement as $utilisateurFormatTableau) {
			$utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
		}

		return $utilisateurs;
	}
}
