<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Test configuration base de donnees</title>
		<meta charset="UTF-8">
	</head>
	<body>
		<?php
		require_once 'ConfigurationBaseDeDonnees.php';
		echo '<p>getNomHote() = ' . ConfigurationBaseDeDonnees::getNomHote() . '</p>';
		echo '<p>getNomBaseDeDonnees() = ' . ConfigurationBaseDeDonnees::getNomBaseDeDonnees() . '</p>';
		echo '<p>getPort() = ' . ConfigurationBaseDeDonnees::getPort() . '</p>';
		echo '<p>getLogin() = ' . ConfigurationBaseDeDonnees::getLogin() . '</p>';
		echo '<p>getPassword() = ' . ConfigurationBaseDeDonnees::getPassword() . '</p>';
		?>
	</body>
</html>
