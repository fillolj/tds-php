<?php
require_once '../src/Lib/Psr4AutoloaderClass.php';

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Controleur\ControleurUtilisateur;

$action = isset($_GET['action']) ? $_GET['action'] : 'afficherListe';

if(in_array($action, get_class_methods('App\Covoiturage\Controleur\ControleurUtilisateur'))) {
    ControleurUtilisateur::$action();
} else {
    ControleurUtilisateur::afficherErreur('La page n\'existe pas.');
}