<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;

class ControleurUtilisateur {
    public static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);
        require __DIR__ . "/../vue/$cheminVue";
    }
    public static function afficherErreur(?string $messageErreur = null) : void {
        $parametres = [
            'titre' => 'Erreur',
            'cheminCorpsVue' => 'utilisateur/erreur.php'
        ];
        if($messageErreur != null) {
            $parametres['messageErreur'] = $messageErreur;
        }
        ControleurUtilisateur::afficherVue('vueGenerale.php', $parametres);
    }
    public static function afficherListe() : void {
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'utilisateurs' => ModeleUtilisateur::recupererUtilisateurs(),
            'titre' => 'Liste d\'utilisateurs',
            'cheminCorpsVue' => 'utilisateur/liste.php'
        ]);
    }
    public static function afficherDetail() : void {
        $utilisateur = null;
        if(isset($_GET['login'])) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
        }

        if($utilisateur != null) {
            ControleurUtilisateur::afficherVue('vueGenerale.php', [
                'utilisateur' => $utilisateur,
                'titre' => 'Détails de ' . $utilisateur->getLogin(),
                'cheminCorpsVue' => 'utilisateur/detail.php'
            ]);
        } else {
            ControleurUtilisateur::afficherErreur('L\'utilisateur n\'existe pas.');
        }
    }
    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'titre' => 'Créer un utilisateur',
            'cheminCorpsVue' => 'utilisateur/formulaireCreation.php'
        ]);
    }
    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();

        ControleurUtilisateur::afficherVue('vueGenerale.php', [
            'utilisateurs' => ModeleUtilisateur::recupererUtilisateurs(),
            'titre' => 'Liste d\'utilisateurs',
            'cheminCorpsVue' => 'utilisateur/utilisateurCree.php'
        ]);
    }
}