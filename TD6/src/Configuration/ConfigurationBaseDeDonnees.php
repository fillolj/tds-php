<?php
namespace App\Covoiturage\Configuration;

class ConfigurationBaseDeDonnees {
    static private array $configurationBaseDeDonnees = array (
        'nomHote' => 'webinfo.iutmontp.univ-montp2.fr',
        'nomBaseDeDonnees' => 'fillolj',
        'port' => 3316,
        'login' => 'fillolj',
        'motDePasse' => '"qem"F%M+kWv_%3'
    );
    static public function getNomHote() : string {
        return ConfigurationBaseDeDonnees::$configurationBaseDeDonnees['nomHote'];
    }
    static public function getNomBaseDeDonnees() : string {
        return ConfigurationBaseDeDonnees::$configurationBaseDeDonnees['nomBaseDeDonnees'];
    }
    static public function getPort() : int {
        return ConfigurationBaseDeDonnees::$configurationBaseDeDonnees['port'];
    }
    static public function getLogin() : string {
        return ConfigurationBaseDeDonnees::$configurationBaseDeDonnees['login'];
    }
    static public function getPassword() : string {
        return ConfigurationBaseDeDonnees::$configurationBaseDeDonnees['motDePasse'];
    }
}