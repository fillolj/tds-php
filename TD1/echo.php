<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title> Mon premier php </title>
	</head>
 
	<body>
		<p>Voici le résultat du script PHP :</p>
		<?php
		// Ceci est un commentaire PHP sur une ligne
		/* Ceci est le 2ème type de commentaire PHP
		sur plusieurs lignes */
		 
		// On met la chaine de caractères "hello" dans la variable 'texte'
		// Les noms de variable commencent par $ en PHP
		$texte = "hello world !";

		// On écrit le contenu de la variable 'texte' dans la page Web
		echo "<p>$texte</p>";

		$utilisateurs = [];
		$utilisateurs[] = ['nom' => "Leblanc", 'prenom' => "Juste", 'login' => "leblancj"];
		$utilisateurs[] = ['nom' => "Bonberre", 'prenom' => "Jean", 'login' => "blblbl"];
		$utilisateurs[] = ['nom' => "#§!", 'prenom' => "Eflpf", 'login' => "€Ÿ"];

		if(count($utilisateurs) > 0) {
			echo "<ul>";
			foreach($utilisateurs as $k => $v)
			{
				echo "<li>Utilisateur $v[prenom] $v[nom] de login $v[login]</li>";
			}
			echo "</ul>";
		} else {
			echo "<p>Aucun utilisateur.</p>";
		}
		?>
	</body>
</html> 
