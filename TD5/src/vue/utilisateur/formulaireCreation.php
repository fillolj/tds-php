<form method="get">
    <fieldset>
        <legend>Créer un utilisateur :</legend>
        <input type="hidden" name="action" value="creerDepuisFormulaire">
        <p class="InputAddOn"><label for="login_id" class="InputAddOn-item">Login*</label><input type="text" placeholder="leblancj" name="login" id="login_id" class="InputAddOn-field" required></p>
        <p class="InputAddOn"><label for="nom_id" class="InputAddOn-item">Nom*</label><input type="text" placeholder="Leblanc" name="nom" id="nom_id" class="InputAddOn-field" required></p>
        <p class="InputAddOn"><label for="prenom_id" class="InputAddOn-item">Prénom*</label><input type="text" placeholder="Juste" name="prenom" id="prenom_id" class="InputAddOn-field" required></p>
        <p><input type="submit" value="Envoyer"></p>
    </fieldset>
</form>