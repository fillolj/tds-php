<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title><?=htmlspecialchars($titre)?> - Covoiturage</title>
        <link rel="stylesheet" href="../ressources/navstyle.css">
    </head>
    <body>
        <header>
            <h1>Covoiturage</h1>
            <nav>
                <ul>
                <li><a href="?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a></li>
                <li><a href="?action=afficherListe&controleur=trajet">Gestion des trajets</a></li>
                </ul>
            </nav>
        </header>
        <main>
            <h2><?=htmlspecialchars($titre)?></h2>
            <?php
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>© Copyright LOLOLOL</p>
        </footer>
    </body>
</html>
