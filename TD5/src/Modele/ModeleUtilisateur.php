<?php
namespace App\Covoiturage\Modele;

use App\Covoiturage\Modele\ConnexionBaseDeDonnees;

class ModeleUtilisateur {
	private string $login;
	private string $nom;
	private string $prenom;

	public function getLogin() : string {
		return $this->login;
	}

	public function setLogin(string $login) {
		$this->login = substr($login, 0, 64);
	}

	public function getNom() : string {
		return $this->nom;
	}

	public function setNom(string $nom) {
		$this->nom = $nom;
	}

	public function getPrenom() : string {
		return $this->prenom;
	}

	public function setPrenom(string $prenom) {
		$this->prenom = $prenom;
	}

	public function __construct(string $login, string $nom, string $prenom) {
		$this->setLogin($login);
		$this->setNom($nom);
		$this->setPrenom($prenom);
	}

	public function __toString() : string {
		return "$this->prenom $this->nom";
	}
	
	public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
		return new ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
	}

	public static function recupererUtilisateurs() : array {
		$utilisateurs = [];

		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->query('select login, nom, prenom from utilisateur');
		foreach($pdoStatement as $utilisateurFormatTableau) {
			$utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
		}

		return $utilisateurs;
	}

	public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
		$sql = "SELECT * from utilisateur WHERE login = :loginTag";
		$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
		$values = [ "loginTag" => $login ];
		$pdoStatement->execute($values);
		$utilisateurFormatTableau = $pdoStatement->fetch();
		if($utilisateurFormatTableau == false) {
			return null;
		}
		return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
	}

	public function ajouter() : bool {
		try {
			$sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
			$pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
			$values = [
				"login" => $this->login,
				"nom" => $this->nom,
				"prenom" => $this->prenom,
			];
			$pdoStatement->execute($values);
			return true;
		} catch (PDOException $e) {
			return false;
		}
	}
}
