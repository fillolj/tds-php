<?php
require_once '../src/Lib/Psr4AutoloaderClass.php';

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . "/../src");

use App\Covoiturage\Controleur\ControleurUtilisateur;

$action = $_GET['action'];
ControleurUtilisateur::$action();