<?php
require_once '../Modele/ModeleUtilisateur.php';
class ControleurUtilisateur {
    public static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);
        require "../vue/$cheminVue";
    }
    public static function afficherListe() : void {
        ControleurUtilisateur::afficherVue('utilisateur/liste.php', [
            'utilisateurs' => ModeleUtilisateur::recupererUtilisateurs()
        ]);
    }
    public static function afficherDetail() : void {
        $utilisateur = null;
        if(isset($_GET['login'])) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
        }

        if($utilisateur != null) {
            ControleurUtilisateur::afficherVue('utilisateur/detail.php', [
                'utilisateur' => $utilisateur
            ]);
        } else {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
        }
    }
    public static function afficherFormulaireCreation() : void {
        ControleurUtilisateur::afficherVue('utilisateur/formulaireCreation.php');
    }
    public static function creerDepuisFormulaire() : void {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        
        ControleurUtilisateur::afficherListe();
    }
}