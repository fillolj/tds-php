<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Liste des utilisateurs</title>
    </head>
    <body>
        <?php
        foreach($utilisateurs as $utilisateur) {
            $login = $utilisateur->getLogin();
            echo "<p>Utilisateur de login $login (<a href=\"?action=afficherDetail&login=$login\">détails</a>).</p>";
        }
        ?>
        <p><a href="?action=afficherFormulaireCreation">Créer un utilisateur</a></p>
    </body>
</html>